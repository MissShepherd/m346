## Vertikale Skalierung 

Abgaben:

1. **Vorher-Nachher Screenshots der Instanz-Ressourcen:**
   Vorher:
   ![Vorher](Pfad/zum/Vorher-Screenshot.png)

   Nachher:
   ![Nachher](Pfad/zum/Nachher-Screenshot.png)

2. **Erklärungen:**
   Erklärungen zu den Veränderungen und Optimierungen, die auf den Screenshots zu sehen sind.
