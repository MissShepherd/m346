## Auto Scaling

# Abgaben:

## 1. Laufende Umgebung der Lehrperson:

### Aktualisierte Cloud-Init-Konfiguration:
   Fügen Sie die aktualisierte Cloud-Init-Konfiguration für die erweiterte Umgebung hinzu. Stellen Sie sicher, dass alle notwendigen Anpassungen dokumentiert sind.

### Screenshots der erweiterten Umgebung:
   Erstellen Sie Screenshots Ihrer erweiterten Umgebung, um die zusätzlichen Objekte, wie Template, Health Checks, usw., zu zeigen.

## 2. Auskunft zu Vorgehen und erstellten Objekten:

### Template:
   Erklären Sie den Zweck und die Funktionalität des erstellten Templates. Welche Ressourcen und Konfigurationen sind darin enthalten?

### Health Checks:
   Geben Sie eine kurze Erklärung zu den Health Checks in Ihrer Umgebung. Welche Dienste oder Komponenten werden überwacht, und wie werden etwaige Probleme behandelt?

### Weitere Anpassungen und Objekte:
   Falls Sie weitere Anpassungen oder Objekte hinzugefügt haben, erläutern Sie bitte kurz deren Funktion und den Grund für ihre Implementierung.

## 3. Bereitschaft zur Auskunft:

### Diskussionsbereitschaft:
   Zeigen Sie Ihre Bereitschaft, Auskunft über Ihr Vorgehen, die erstellten Objekte und mögliche Optimierungen zu erteilen. Sollten Fragen auftreten, stehe ich zur Verfügung, um diese zu klären.

### Interaktive Präsentation:
   Falls eine interaktive Präsentation gewünscht ist, stehe ich gerne zur Verfügung, um live durch die Umgebung zu führen und spezifische Aspekte zu erläutern.
