## Installation App


Abgaben:

1. **Reverse Proxy:**
   Kurze Erklärung in eigenen Worten, was ein Reverse Proxy ist.

2. **Swagger-URL Aufruf:**
   ![Swagger-URL Aufruf](Pfad/zum/Screenshot1.png)
   Screenshot, der zeigt, dass Sie die Swagger-URL aufrufen können.

3. **Aufruf des Endpoints GetProducts:**
   ![Aufruf des Endpoints GetProducts](Pfad/zum/Screenshot2.png)
   Screenshot, der zeigt, dass Sie den Endpoint GetProducts aufrufen können (via Swagger) und auch ein korrektes Resultat bekommen.

4. **MongoDB Collection:**
   ![MongoDB Collection](Pfad/zum/Screenshot3.png)
   Screenshot einer der MongoDB Collections mit Auszug aus dem Inhalt.
