## Horizontale Skalierung

Abgaben:

1. **Erklärungen:**
   Erklärungen zu den Veränderungen und Optimierungen, die in Ihrer Umgebung vorgenommen wurden. Beschreiben Sie, was auf den Screenshots zu sehen ist und warum bestimmte Anpassungen vorgenommen wurden.

2. **Hochladen in Git:**
   Fügen Sie die Screenshots und diese Markdown-Datei Ihrem Git-Repository hinzu und machen Sie einen Commit mit einer aussagekräftigen Nachricht. Synchronisieren Sie anschließend Ihr Repository, um sicherzustellen, dass die Änderungen online verfügbar sind.

3. **Lehrperson-Präsentation:**
   Stellen Sie sicher, dass Ihre laufende Umgebung der Lehrperson zugänglich ist, damit sie die durchgeführten Änderungen überprüfen kann.
