## Objekte und Instanzen erstellen

Nachdem Sie sich nun auf das Subnetz und IPs festgelegt haben, können Sie die restlichen Objekte
anlegen.
In KN03 wurden automatisch Security Groups erstellt. Das Problem dabei ist, dass diese
keine sinnvollen Namen besitzen und so kaum wiedererkannt werden. Erstellen sie nun also
zwei Security Groups (für Web und DB). Die Ports sind die gleichen wie bei KN03. Geben Sie
sinnvolle Werte für die Felder Name und Security group name.
Ändern Sie die Security Group für die DB und lassen Sie für den Port 3306 nur Zugriff
innerhalb des eigenen Subnetzes zu.
Erstellen Sie nun die beiden Instanzen für den Web- und DB-Server. Sie können die gleichen
Cloud-Init-Dateien verwenden, aber müssen nun die IP anpassen für den Datenbankzugriff.
Diese IP wird sich aber - im Gegensatz zu KN03 - nicht mehr ändern. Wenn Sie die Instanzen
erstellen, müssen Sie die Netzwerk-Einstellungen genauer betrachten und wählen Sie die
korrekten Objekte aus (Subnetz, IP, SG). Achtung: Zu diesem Zeitpunkt definieren Sie nur die
private Adresse.
Erstellen Sie zwei öffentliche IP-Adressen (Elastic IPs). Geben Sie wieder sinnvolle Werte für
das Feld Name. Weisen Sie die IP-Adressen den beiden erstellten Instanzen zu (über das
Aktionen-menu).
Stoppen Sie die beiden Instanzen. Zeigen Sie, dass die IPs nicht verloren gehen, wenn die
Instanz gestoppt ist.

Abgaben:

1. Screenshot der Liste der Sicherheitsgruppe mit sprechenden Namen/Feldern ![Screenshot der Subnetze](/Screenshots/Sicherheitsgruppe.png)
2. Screenshot der Inbound-Regel für die DB-Sicherheitsgruppe ![Screenshot der Subnetze](/Screenshots/DB-Sicherheitsgruppe.png)
3. Screenshot der Liste der Elastic IPs mit sprechenden Namen ![Screenshot der Subnetze](/Screenshots/Elastic%20IPs.png)
4. Zeigen Sie, dass Sie nun alle drei Seiten aufrufen können (index.html, info.php und db.php).
- ![Screenshot der Subnetze](/Screenshots/KN05%20Index.png)
- ![Screenshot der Subnetze](/Screenshots/KN05%20info.png)
- ![Screenshot der Subnetze](/Screenshots/KN05%20db.png)
5. Screenshot der Liste der Instanzen, wenn beide Instanzen gestoppt sind. Das Feld der
   öffentlichen IP und der Status (gestoppt) müssen sichtbar sein ![Screenshot der Subnetze](/Screenshots/gestoppt.png)
6. Screenshot der Details beider Instanzen, sodass die Subnet ID sichtbar ist ![Screenshot der Subnetze](/Screenshots/web%20subnet.png)
![Screenshot der Subnetze](/Screenshots/db%20subnet.png)
7. Laden Sie die aktualisierten Cloud-init Konfigurationen hoch in Git. Im Web-Konfig, sollte die
   IP angepasst sein.
