## Diagramm erstellen

Im Modul 117 hatten Sie ein Diagramm über Netzwerke gezeichnet. Dies werden wir nun für Ihre AWS
Umgebung wiederholen. Folgende Inhalte muss Ihr Diagramm zeigen:

1. Ihr VPC (Virtual Private Cloud) mit dem IP Range / CIDR Block
2. Alle bestehenden Subnetze mit dem IP Range / CIDR Block.

Wissen Sie in welchem Subnetz Ihre Instanzen aus den vorherigen KNs lagen? Falls Sie Ihre
Objekte schon gelöscht haben, können Sie einfach das Formular für eine neue Instanz
aufrufen und nachschauen was per Default ausgewählt war. Zeichnen Sie die VMs, die Sie in
KN03 benötigten im entsprechenden Subnetz ein. Die VMs müssen dazu nicht mehr
existieren, sei können ja einfach die Namen "KN03-DB" und "KN03-Web" verwenden.

Das Ziel ist, dass Sie Ihre Umgebung verstehen.

Abgaben:
Diagramm als Bild ![Screenshot des Diagrams](/Screenshots/Diagramm.png)
