## Subnetz und private IP wählen

Sie haben nun herausgefunden, in welchem Subnetz sich Ihre Server aus KN03 befanden. Geben
Sie dem Subnetz einen Namen (z.B. Subnet-KN03).
Wählen Sie ein beliebiges anderes Subnetz und geben Sie ihm ebenfalls einen Namen für diese
aktuelle KN (z.B. Subnet-KN05).
In den folgenden Teilaufgaben werden Sie die Schritte für KN03 wiederholen mit ein paar
Zusätzen. Dazu wählen Sie nun zwei IPs aus dem Subnetz, die Sie dann später Ihren Instanzen
zuweisen werden (private IPs).

Abgaben:

- Screenshot der Subnetzen, die die Namen zeigen. ![Screenshot der Subnetzen](/Screenshots/Subnetzen.png)
- Zwei definierte IPs für Web- und DB-Server/Instanz. 
- Webserver: 172.31.80.55
- DB-Server/Instanz: 172.31.80.13