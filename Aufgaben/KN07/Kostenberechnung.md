# Abgaben für Kostenrechnung

## A) Kostenrechnung erstellen

### 1) Rehosting

Für die Option Rehosting beschränkt sich die Firma auf die beiden public cloud Anbieter AWS und Azure. Verwenden Sie die Kostenrechner (oben verlinkt) der beiden Anbieter um folgende Anforderungen zu berechnen:

1. Web Server mit 1 Core, 20GB Speicher, 2 GB RAM, Ubuntu
2. DB Server mit 2 Cores, 100GB Speicher, 4 GB RAM, Ubuntu
3. Load Balancer für zukünftige Skalierbarkeit
4. Backup-Speicher für die Datenbank Daten:
   - Tägliche Backups für die letzten 7 Tage
   - Wöchentliche Backups für den letzten Monat
   - Monatliche Backups für die letzten drei Monate.

**Abgabe:**
**Anbieter AWS:**
![Screenshot der Kostenrechnungen für Rehosting](/Screenshots/Anbieter%20AWS.png)
- Ich habe mich für Amazon RDS for MySQL, Amazon EC2 und AWS Backup entschieden, weil sie eine robuste und skalierbare Lösung bieten. Amazon RDS automatisiert wichtige Datenbankverwaltungsaufgaben, wodurch der Betrieb effizienter wird. Amazon EC2 bietet Flexibilität und Kontrolle über die Serverkonfiguration, während AWS Backup eine zuverlässige Methode zur Sicherung und Wiederherstellung meiner Daten bereitstellt. Diese Kombination erfüllt die Anforderungen meiner Anwendung effektiv und ermöglicht es mir, mich auf die Entwicklung zu konzentrieren. <br>
**Anbieter Azure**
![Screenshot der Kostenrechnungen für Rehosting](/Screenshots/Azure%20Screenshot%20Web.png)
![Screenshot der Kostenrechnungen für Rehosting](/Screenshots/Azure%20Screenshot%20DB.png)
![Screenshot der Kostenrechnungen für Rehosting](/Screenshots/Azure%20Gesamt.png)
- Für meinen Datenbankserver habe ich mich für eine A2-Instanz mit 2 Kernen, 4 GB RAM und 20 GB Speicher sowie für eine Gen5-Instanz mit 2 Kernen entschieden. Diese Auswahl erfüllt meine aktuellen Anforderungen optimal. Die A2-Instanz bietet ausreichend Leistung für meine Anwendung, während die Gen5-Instanz zusätzliche Leistung ermöglicht, falls sie benötigt wird. Mit 100 GB Speicherplatz für meine Daten bin ich gut ausgestattet, und die 2 TB Backup-Speicher gewährleisten die Sicherheit meiner Daten. Diese Entscheidung ermöglicht zukünftige Skalierbarkeit und stellt insgesamt eine ausgewogene Lösung für meine Anforderungen dar.

### 2) Replatforming

Ihre Firma möchte Heroku als Plattform benutzen. Kosten für die Entwicklung/Veränderung der Software werden ignoriert. Verwenden Sie die gleichen Parameter wie bei 1) als Grundlage.

**Abgabe:**
![Screenshot der Kostenrechnung für Replatforming](/Screenshots/Replatforming.png)
- Wir haben uns für die Specifications entschieden, da die am ehesten unseren Standards entsprechen. Es ist allerdings sehr schwammig formuiert, was was ist. Hier betragen die Gesamtkosten 3000$

### 3) Repurchasing

Ihre Firma möchte die Software ablösen mit einem der beiden Anbieter Zoho CRM oder SalesForce Sales Cloud. Ihre Firma hat 16 Mitarbeiter, die das CRM System benutzen werden.

**Abgabe:** <br>
 **Zoho** <br>
![Screenshot des Pricings von Zoho CRM](/Screenshots/Zoho%20CRM.png) 

**SalesForce Sales Cloud** <br>
![Screenshot des Pricings von SalesForce Sales Cloud](/Screenshots/SalesForce%20Sales%20Cloud.png)

Angesichts der verschiedenen Möglichkeiten zur Software-Ersetzung ergeben sich folgende Überlegungen:

- AWS bietet mit Rehosting Kontrolle, Skalierbarkeit und eine kosteneffiziente Infrastruktur, während Azure tendenziell teurer ist.

- Heroku als Plattform as a Service (PaaS) verringert den Verwaltungsaufwand, ist jedoch im Vergleich zu AWS kostenintensiver.

- SaaS-Optionen wie Zoho CRM und Salesforce Sales Cloud sind leicht zu bedienen, aber mit höheren Kosten verbunden.

Die Entscheidung hängt von Faktoren wie Budget, technischem Know-how und spezifischem Anwendungsfall ab. Rehosting mit AWS könnte bei begrenztem Budget und begrenzter technischer Expertise die wirtschaftlichste Wahl sein, während SaaS-Lösungen zwar sofort einsatzbereit sind, aber tendenziell teurer ausfallen können.


## B) Interpretation der Resultate

Interpretieren Sie die Resultate der Varianten u.a. mit den folgenden Fragen:

- Wie stark unterscheiden sich die Angebote?
- Welches ist das billigste?
- Wieso ist eines davon viel teurer? Ist es aber wirklich teurer?

Rehosting mit AWS ist die kostengünstigste Option, mit jährlichen Kosten von 2554.20$. Es bietet Kontrolle und Skalierbarkeit, könnte jedoch einige Verwaltungsaufwände erfordern.

Azure ist teurer als AWS (4927.56$ pro Jahr), bietet jedoch ähnliche technische Infrastruktur. Die höheren Kosten könnten auf unterschiedliche Preismodelle und Konfigurationen zurückzuführen sein.

Replatforming mit Heroku ist bequem, erfordert jedoch höhere Kosten von 3000$ monatlich (36'000$ jährlich). Es reduziert den Verwaltungsaufwand, könnte aber teurer sein als Rehosting.

Repurchasing mit Zoho CRM kostet 640 Euro pro Monat, während Salesforce Sales Cloud 2640 Euro pro Monat kostet. Salesforce ist teurer, bietet jedoch erweiterte Funktionen und einen größeren Umfang.

Ein teureres Angebot muss nicht zwingend "teurer" sein. Es kann vielleicht sogar ein besseres Angebot sein, wenn es die Arbeitsbelastung reduziert oder mehr Funktionen bietet. Deswegen ist es wichtig, die Kosten im Verhältnis zu der Ausgangslage und Mindestanforderungen zu betrachten.
