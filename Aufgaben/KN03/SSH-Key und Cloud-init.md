- **Ihre angepasste Cloud-init Konfiguration als Datei im Git-Repository**: Siehe Datei in der Übersicht

- **Ein Screenshot der Details oder Liste der Instanz, welcher den verwendeten Key zeigt**:
  ![Instanz Key Screenshot](/Screenshots/verwendeten%20Key.png)

- **Screenshot mit dem ssh-Befehl und des Resultats unter Verwendung des ersten Schlüssels**:
  ![SSH mit erstem Schlüssel Screenshot](/Screenshots/ersten%20Schlüssels.png)

- **Screenshot mit dem ssh-Befehl und des Resultats unter Verwendung des zweiten Schlüssels**:
  ![SSH mit zweitem Schlüssel Screenshot](/Screenshots/zweiten%20Schlüssels.png )

- **Screenshot mit dem Auszug aus dem Cloud-Init-Log**:
  ![Cloud-Init-Log Screenshot](/Screenshots/Auszug.png)
