## Beweisführung DB Server

1. **Verbinden Sie sich mit dem Server (Shell) und zeigen Sie, dass die Datenbankverbindung mit dem Benutzer admin funktioniert.**
   - Der Befehl dazu lautet: `mysql -u admin -p`
   - Sie müssen dann anschließend das Passwort eingeben.
   - Erstellen Sie einen Screenshot, der den Befehl und die CLI von mysql zeigt.

   ![DB Server Verbindung](/Screenshots/Datenbankverbindung.png)

2. **Zeigen Sie von Ihrem lokalen System aus, dass Sie von extern auf den Datenbank Server zugreifen können.**
   - Verwenden Sie dazu telnet mit dem Befehl: `telnet <IP> 3306`
   - Erstellen Sie einen Screenshot des Befehls und des Ergebnisses.
   - Sie können den Telnet-Client über die Windows-Features aktivieren. Wenn Sie kryptische Zeichen als Antwort bekommen, funktioniert der Zugriff.

   ![DB Server Externer Zugriff](/Screenshots/telnet.png)

## Beweisführung Webserver

1. **Rufen Sie die Seiten `info.php` und `db.php` auf und erstellen Sie einen Screenshot der URL und des Inhalts.**

   ![Webserver Seite aufrufen info](/Screenshots/info%20php.png)
   ![Webserver Seite aufrufen db](/Screenshots/webserver%20db.png)

2. **Rufen Sie Adminer auf (http://ihre-ip/adminer/), verbinden Sie sich mit dem DB-Server und zeigen Sie mit Screenshots, dass die Verbindung funktioniert.**

   ![Adminer Verbindung](/Screenshots/adminer.png)
