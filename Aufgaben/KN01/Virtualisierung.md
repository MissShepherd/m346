# KN01 -  Virtualisierung

Dieses Repository enthält Screenshots und Dokumentationen für das Modul "M346 Cloud-Lösungen konz. u. real." von Tatjana Schäfer.

## Tatjana Schäfer

In diesem Abschnitt finden Sie Screenshots und Dokumentationen von Tatjana Schäfer für das Modul M346 Cloud-Lösungen konz. u. real.

### Ausgabe der CPUs bei weniger CPU als das Host-System

![Screenshot 1](/Screenshots/KN01%20read%20RAM%20before%20change.png)

Das ist die Ausgabe vom Linux Server, bei der CPU-Abfrage, sofern die Anzahl Prozessoren kleiner oder gleich groß ist wie die des Hostsystems.

---

### Ausgabe der CPUs (oder Fehlermeldung des Host-Systems) bei mehr CPU als Ihr Host-System

![Screenshot 2](/Screenshots/KN01%20Error%20changing%20RAM%20bigger%20than%20Host.png)

Dies ist die Fehlermeldung vom Hostsystem (VMWare), wenn die Anzahl virtueller Prozessoren die der physischen logischen Prozessoren übersteigt.

----

### Ausgabe des RAM bei weniger RAM als Ihr Host-System

![Screenshot 3](/Screenshots/KN01%20read%20RAM%20before%20change.png)

Das ist die Ausgabe der Konsole, wenn der RAM kleiner oder gleich dem RAM des Host-PCs ist.

---

### Ausgabe des RAM (oder Fehlermeldung des Host-Systems) bei mehr RAM als Ihr Host-System

![Screenshot 4](/Screenshots/KN01%20Error%20changing%20RAM%20bigger%20than%20Host.png)

Das ist die Fehlermeldung, wenn man versucht, dem Guest-System mehr RAM zuzuweisen, als das Hostsystem hat.