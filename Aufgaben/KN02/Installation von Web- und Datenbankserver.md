- **Diese URL zeigt ihnen die Informationen von Apache an.**
  ![Erster Schlüssel Screenshot](/Screenshots/index.png)

- **Diese URL zeigt ihnen die Informationen von PHP an.**
  ![Zweiter Schlüssel Screenshot](/Screenshots/info.png)

- **Diese URL zeigt ihnen die Datenbank-Benutzer.**
  ![Web Server-Instanz Screenshot](/Screenshots/db.png)