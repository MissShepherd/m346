- **Liste der Buckets**
  ![HTML-Seite Screenshot](/Screenshots/Buckets.png)

- **HTML-Seite, inkl. URL**
  ![Liste der EC2-Instanzen Screenshot](/Screenshots/HTML-Seite.png)

- **Liste der Dateien im Bucket**
  ![Web Server-Instanz Screenshot](/Screenshots/Dateien.png)

- **Eigenschaften von "Static website hosting**
  ![Security-Group Inbound-Regeln Screenshot](/Screenshots/Eigenschaften.png)