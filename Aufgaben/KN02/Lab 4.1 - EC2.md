- **HTML-Seite, inkl. URL**
  ![HTML-Seite Screenshot](/Screenshots/Hello%20World.png)

- **Liste der EC2-Instanzen**
  ![Liste der EC2-Instanzen Screenshot](/Screenshots/E2-Instanz.png)

- **Details der Web Server-Instanz (öffentliche IP sichtbar)**
  ![Web Server-Instanz Screenshot](/Screenshots/Server-Instanz.png)

- **Security-Group: Liste der Inbound-Regeln**
  ![Security-Group Inbound-Regeln Screenshot](/Screenshots/Security-Group.png)
