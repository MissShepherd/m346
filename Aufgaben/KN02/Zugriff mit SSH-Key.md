- **Screenshot mit dem ssh-Befehl und des Resultats unter Verwendung des ersten Schlüssels**
  ![Erster Schlüssel Screenshot](/Screenshots/ersten%20schluessel.png)

- **Screenshot mit dem ssh-Befehl und des Resultats unter Verwendung des zweiten Schlüssels**
  ![Zweiter Schlüssel Screenshot](/Screenshots/zweiter%20schuessel.png)

- **Screenshot der Instanz-Detail (oder Liste), so dass der verwendete Schlüssel sichtbar ist.**
  ![Web Server-Instanz Screenshot](/Screenshots/Instanz-Detail.png)