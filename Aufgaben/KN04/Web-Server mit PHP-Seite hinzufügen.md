## Web-Server mit PHP-Seite hinzufügen

**Machen Sie die folgenden Änderungen:**

1. Ändern Sie Ihren Namen

2. Ersetzen Sie den Text "Hier kommt Ihr Text." mit einem Text, der das Bild beschreibt

3. Ersetzen Sie den Text "URL-To-The-Image" mit der URL auf Ihr Bild.

4. Fügen Sie die Quellenangabe hinzu, falls notwendig (und ersetzen Sie den Text).

**Erstellen Sie einen Web-Server (ohne Datenbank-Server). Verwenden Sie das Cloud-Init-File aus KN03, aber ersetzen Sie die Dateien, die geschrieben werden mit dem angepassten Inhalt von oben. Räumen Sie auch gleich auf. Entfernen Sie die Pakete und Befehle, die nicht benötigt werden.**

**Abgaben:**

1. Neues Cloud-Init für die Web-Instanz

2. Screenshot der Seite image.php (mit sichtbarer URL)
   ![Screenshot der Seite image.php](Pfad/zum/Screenshot.jpg)
