## Elastic Block Storage (EBS) hinzufügen

**1. Erstellen Sie ein neues Volume mit 30GB in der AWS Konsole. Suchen Sie selbstständig nach Informationen dazu.**

**2. Fügen Sie das neue Volumen der bestehenden Instanz hinzu. ACHTUNG: der Mount-Pfad ist wichtig. Lesen Sie sich ein was /dev/sda1 und /dev/sdf bedeutet.**

**3. Erstellen Sie ein Screenshot der Details ihrer Instanz, der beide EBS-Disks zeigt.**

Abgaben:

- Screenshot der Liste der EBS (2 Volumen) der Instanz. Alle Spalten sollen sichtbar sein.
   ![Screenshot der EBS-Liste](/Screenshots/Liste%20der%20EBS.png)
