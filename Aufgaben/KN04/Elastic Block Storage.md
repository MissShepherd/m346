##  Speichereigenschaften erkennen

Ihre Instanz verwendet nun 2 EBS und 1 S3 Speicher. Kategorisieren Sie diese nach den folgenden
Eigenschaften.
Speichermodell: Hot, Warm, Cold
Persistenz. Schauen Sie sich den Screenshot aus Teil C) an, um diese Frage zu beantworten.

## Abgaben
### Kategorisierung
|                            | Typ         | Persistenz| 
|----------------------------|--------------|----------|
| EBS Root                   | hot          | nein     |
| EBS Zusätzliches Volumen   | hot          | ja       |
| S3                         | hot/warm     | ja       |

### Erklärung/Begründung zu der Kategorisierung.

Die beiden EBS-Instanzen sind als Hot-Storages konfiguriert, da sie oft und schnell darauf zugegriffen werden müssen. Sie sind jedoch nicht persistent, da sie gelöscht werden, wenn die EC2-Instanz, an die sie angehängt sind, gelöscht wird.

Der S3-Storage kann je nach Konfiguration und Häufigkeit des Zugriffs entweder als Warm- oder sogar als Hot-Storage fungieren. Dieser Speicher ist persistent, da er nicht direkt an eine EC2-Instanz gebunden ist und nicht von ihr abhängt.
