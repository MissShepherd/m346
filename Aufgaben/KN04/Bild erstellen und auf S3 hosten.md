## Bild erstellen und auf S3 hosten

Oft werden statische Bilder (z.B. Logos), die sich nicht ändern in einem S3-Bucket platziert und von dort in der Web-App referenziert. Wir werden dieses Variante nun testen.

1. Erstellen Sie wieder ein S3-Bucket wie Sie es in KN02 schon getan haben. Der Name des Buckets muss ihren Namen enthalten. Als Objekt werden Sie nun aber ein Bild hochladen. Achtung: Dieses Bild sollte ihnen gehören (z.B. von Ihrem Handy) oder kosten- und lizenzfrei sein (z.B. unsplash.com).

**Abgaben:**

1. Screenshot der S3-Objekte im Bucket
   ![Screenshot der S3-Objekte im Bucket](/Screenshots/Objekte%20im%20Bucket.png)

2. Screenshot des Bildes im Browser (mit sichtbarer URL)
   ![Screenshot des Bildes im Browser](/Screenshots/Bildes%20im%20Browser.png)
