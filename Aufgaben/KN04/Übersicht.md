- [**Bild erstellen und auf S3 hosten**](/Aufgaben/KN04/Bild%20erstellen%20und%20auf%20S3%20hosten.md): Bild erstellen und auf S3 hosten

- [**Web-Server mit PHP-Seite hinzufügen**](/Aufgaben/KN04/Web-Server%20mit%20PHP-Seite%20hinzufügen.md): Detaillierte Aufgabenstellung für die Aufgabe KN04

- [**Elastic Block Storage (EBS) hinzufügen**](/Aufgaben/KN04/Web-Server%20mit%20PHP-Seite%20hinzufügen.md): Elastic Block Storage (EBS) hinzufügen

- [**Speichereigenschaften erkennen**](/Aufgaben/KN04/Speichereigenschaften%20erkennen.md): Speichereigenschaften erkennen
