# Readme - Modul M346 Cloud-Lösungen konz. u. real.

Willkommen im Repository für das Modul "M346 Cloud-Lösungen konz. u. real."! In diesem Repository finden Sie wichtige Ressourcen, Dokumentationen und Dateien, die im Rahmen dieses Moduls verwendet werden. Bitte folgen Sie den untenstehenden Links, um auf spezifische Dateien oder Dokumentationen zuzugreifen:

- [**Aufgabe KN01**](/Aufgaben//KN01/Virtualisierung.md): Hier finden Sie die detaillierte Aufgabenstellung für die Aufgabe KN01.

- [**Aufgabe KN02 - Übersicht**](Aufgaben/KN02/Übersicht.md): Detaillierte Aufgabenstellung für die Aufgabe KN02.


- [**Aufgabe KN03 - Übersicht**](Aufgaben/KN03/Übersicht.md): Detaillierte Aufgabenstellung für die Aufgabe KN03.

- [**Aufgabe KN04 - Übersicht**](Aufgaben/KN04/Übersicht.md): Detaillierte Aufgabenstellung für die Aufgabe KN04.

- [**Aufgabe KN05 - Übersicht**](Aufgaben/KN05/Übersicht.md): Detaillierte Aufgabenstellung für die Aufgabe KN05.

- [**Aufgabe KN06**](Aufgaben/KN06/Übersicht.md): Detaillierte Aufgabenstellung für die Aufgabe KN06.

- [**Aufgabe KN07 - Übersicht**](Aufgaben/KN07/Übersicht.md): Detaillierte Aufgabenstellung für die Aufgabe KN07.